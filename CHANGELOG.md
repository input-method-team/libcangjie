# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

(no changes yet)


## [1.4.0] - 2024-09-01

### Added

- function cangjie_get_radicals():
  to convert a string of multiple codes into the radicals equivlant.
  ([Koala Yeung][yookoala])
- function cangjie_get_codes_by_character():
  to reverse lookup codes of a character of the specified cangjie version.
  ([Koala Yeung][yookoala])
- function libcangjie_check_version_gte():
  to check libcangjie library version. ([Koala Yeung][yookoala],
  [Mathieu Bridon][bochecha])
- function libcangjie_version():
  to get the version string of the current library. ([Koala Yeung][yookoala])
- [CONTRIBUTING.md](CONTRIBUTING.md) to detail development and contribution
  related information. ([Koala Yeung][yookoala], [Mathieu Bridon][bochecha])
- function cangjie_get_characters_v2():
  accepts wildcard characters to be used at the beginning or the end of the
  cangjie code. ([Mathieu Bridon][bochecha])
- added cppcheck, clang-format, and clang-tidy to our CI pipeline to ensure
  code quality and consistancy going forward. ([Koala Yeung][yookoala])

### Changed

- The minimum meson version is now 1.3.2, which is in Ubuntu LTS 24.04.
  ([Mathieu Bridon](bochecha))
- renamed command line tool libcangjie_cli to libcangjie-cli.
  ([Koala Yeung][yookoala], [Mathieu Bridon][bochecha])
- renamed command line tool libcangjie_bench to libcangjie-bench.
  ([Koala Yeung][yookoala], [Mathieu Bridon][bochecha])
- libcangjie-cli previous behaviour is delegated to a subcommand "libcangjie-cli
  query". A new subcommand "libcangjie-cli version" will reveal the current
  library version. ([Koala Yeung][yookoala], [Mathieu Bridon][bochecha])
- updated meson and GitLab CI for better MR handling, code formatting
  operations. ([Koala Yeung][yookoala], [Mathieu Bridon][bochecha])
- unit test coverage improvements. ([Koala Yeung][yookoala],
  [Mathieu Bridon][bochecha])


## [1.3.2] - 2024-07-06

The meson release. ([Mathieu Bridon][bochecha])

We have now finally moved to meson completely, so no more of those awful
crapload of shell scripts with m4 uglyness aka the GNU Autotools.  🎉


## [1.3] - 2014-12-28

This is a data-only bug-fix release:
- Removed some wrong Cangjie codes. They were codes for some Simplified Chinese
  characters, which had been added incorrectly to the Traditional Chinese
  characters, as a (wrong) way to map Traditional to Simplified Chinese.
  ([Koala Yeung][yookoala])
- Fixed a few incorrect Cangjie 3 codes. These just weren't following proper
  Cangjie decomposition rules. ([Koala Yeung][yookoala])
- Added a missing Cangjie 3 code. ([Koala Yeung][yookoala])
- Added Gentoo installation instruction. ([Brendan Horan][brendanhoran])

All in all, these should improve the inputting experience of users, and doesn't
break the ABI/API.

As a result, we recommend upgrading for all users.


## [1.2] - 2014-04-22

This is a minor release.

### Bug Fixes
- Handle failures in the libcangjie_cli tool correctly, instead of crashing a
  segmentation fault. ([Sam][linquize])
- Fixed a couple of typos in the libcangjie_cli tool which prevented it from
  working properly for some filter values. ([Mathieu Bridon][bochecha])
- Use /bin/sh for the autogen.sh script, to improve cross-platform
  compatibility. ([Sam][linquize])
- Fixed a bunch of issues in our Cangjie code data. ([Mathieu Bridon][bochecha],
  Koala Yeung, [Wan Leung Wong][wanleung], [Anthony Ho][anthonyho])

### Docs Improvements
- Added installation instructions for Arch Linux (Antony Ho) and NixOS.
  ([Sam][linquize])
- Removed an obsolete (and confusing) notice about release tarballs.
  ([Mathieu Bridon][bochecha])
- Added missing hiragana filter to the libcangjie_cli help.
  ([Mathieu Bridon][bochecha])


## [1.1] - 2014-02-02

- Fix typo in data/README.table.rst ([Sam][linquize])
- Add docs to the tarball ([Mathieu Bridon][bochecha])
- Add a link to the release tarballs in the README ([Mathieu Bridon][bochecha])
- Correct typo of README file ([Anthony Ho][anthonyho])
- Improve the benchmark tool ([Mathieu Bridon][bochecha])
- Add a missing copyright header to the benchmark tool
  ([Mathieu Bridon][bochecha])
- Various code improvements (Mathieu Bridon, with help from clang's scan-build)
- Code refactoring of the filter handling ([Dridi Boukelmoune][dridi])
- Add install instructions for a few distributions ([Mathieu Bridon][bochecha])
- Document libcangjie_cli ([Mathieu Bridon][bochecha])
- README document fix ([Anthony Wong][anthonywong])


## [1.0] - 2013-12-21

Our first stable release.


[anthonyho]: https://github.com/antonyho
[anthonywong]: https://github.com/anthonywong
[bochecha]: https://gitlab.freedesktop.org/bochecha
[brendanhoran]: https://github.com/brendanhoran
[dridi]: https://github.com/dridi
[linquize]: https://github.com/linquize
[wanleung]: https://github.com/wanleung
[yookoala]: https://gitlab.freedesktop.org/yookoala


[Unreleased]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/compare/v1.4.0...HEAD
[1.4.0]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/compare/v1.3.2...v1.4.0
[1.3.2]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/compare/v1.3...v1.3.2
[1.3]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/compare/v1.2...v1.3
[1.2]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/compare/v1.1...v1.2
[1.1]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/compare/v1.0...v1.1
[1.0]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/tags/v1.0


---

Note: This document trys to follow the [keepachangelog.com][keepachangelog]
suggested format.

[keepachangelog]: https://keepachangelog.com/en/1.0.0/
