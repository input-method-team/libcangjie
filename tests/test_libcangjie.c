#include <assert.h>
#include <libcangjie.h>
#include <string.h>

static void test_libcangjie_version(void) {
    assert(strcmp(libcangjie_version(), LIBCANGJIE_VERSION) == 0);
}

static void test_libcangjie_check_version_get(void) {
    assert(libcangjie_check_version_gte(LIBCANGJIE_VERSION_MAJOR + 1, 0, 0) ==
           1);
    assert(libcangjie_check_version_gte(LIBCANGJIE_VERSION_MAJOR,
                                        LIBCANGJIE_VERSION_MINOR + 1, 0) == 1);
    assert(libcangjie_check_version_gte(LIBCANGJIE_VERSION_MAJOR,
                                        LIBCANGJIE_VERSION_MINOR,
                                        LIBCANGJIE_VERSION_PATCH + 1) == 1);
    assert(libcangjie_check_version_gte(LIBCANGJIE_VERSION_MAJOR,
                                        LIBCANGJIE_VERSION_MINOR,
                                        LIBCANGJIE_VERSION_PATCH) == 1);
    assert(libcangjie_check_version_gte(LIBCANGJIE_VERSION_MAJOR - 1,
                                        LIBCANGJIE_VERSION_MINOR,
                                        LIBCANGJIE_VERSION_PATCH) == 0);
    assert(libcangjie_check_version_gte(LIBCANGJIE_VERSION_MAJOR,
                                        LIBCANGJIE_VERSION_MINOR - 1,
                                        LIBCANGJIE_VERSION_PATCH) == 0);
    assert(libcangjie_check_version_gte(LIBCANGJIE_VERSION_MAJOR,
                                        LIBCANGJIE_VERSION_MINOR,
                                        LIBCANGJIE_VERSION_PATCH - 1) == 0);
}

int main(int argc, const char *argv[]) {
    test_libcangjie_version();
    test_libcangjie_check_version_get();
}
