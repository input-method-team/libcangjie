﻿/* Copyright (c) 2013 - The libcangjie authors.
 *
 * This file is part of libcangjie.
 *
 * libcangjie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libcangjie is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libcangjie.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <cangjie.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>  // For the setenv function
#include <string.h>

#include "config.h"

static void set_env_var(const char *name, const char *value) {
#ifdef _WIN32
    char *buf = (char *)calloc(strlen(name) + strlen(value) + 2, 1);
    strcpy(buf, name);
    strcat(buf, "=");
    strcat(buf, value);
    _putenv(buf);
#else
    setenv(name, value, 1);
#endif
}

static void test_cangjie_new(void) {
    Cangjie *cj;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    cangjie_free(cj);
}

static void test_cangjie_new__with_all_filters(void) {
    Cangjie *cj;

    // Applying all filters to the query
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3,
                          CANGJIE_FILTER_BIG5 | CANGJIE_FILTER_HKSCS |
                              CANGJIE_FILTER_PUNCTUATION |
                              CANGJIE_FILTER_CHINESE | CANGJIE_FILTER_ZHUYIN |
                              CANGJIE_FILTER_KANJI | CANGJIE_FILTER_KATAKANA |
                              CANGJIE_FILTER_HIRAGANA | CANGJIE_FILTER_SYMBOLS);
    assert(ret == CANGJIE_OK);

    // Check if all filters are applied
    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags & CANGJIE_FILTER_BIG5);
    assert(cj->filter_flags & CANGJIE_FILTER_HKSCS);
    assert(cj->filter_flags & CANGJIE_FILTER_PUNCTUATION);
    assert(cj->filter_flags & CANGJIE_FILTER_CHINESE);
    assert(cj->filter_flags & CANGJIE_FILTER_ZHUYIN);
    assert(cj->filter_flags & CANGJIE_FILTER_KANJI);
    assert(cj->filter_flags & CANGJIE_FILTER_KATAKANA);
    assert(cj->filter_flags & CANGJIE_FILTER_HIRAGANA);
    assert(cj->filter_flags & CANGJIE_FILTER_SYMBOLS);

    cangjie_free(cj);
}

static void test_cangjie_new__without_big5(void) {
    Cangjie *cj;

    // Applying all filters to the query
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_HKSCS);
    assert(ret == CANGJIE_OK);

    // Check if all filters are applied
    assert(cj->version == CANGJIE_VERSION_3);
    assert((cj->filter_flags & CANGJIE_FILTER_BIG5) == 0);
    assert(cj->filter_flags & CANGJIE_FILTER_HKSCS);

    cangjie_free(cj);
}

static void test_cangjie_get_characters__single_result(void) {
    Cangjie *cj;
    CangjieCharList *l;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters(cj, "dmlm", &l);
    assert(ret == CANGJIE_OK);

    assert(strcmp(l->c->chchar, "\xE6\xA4\x8F") == 0);  // 椏
    assert(strcmp(l->c->code, "dmlm") == 0);
    assert(l->c->frequency == 10253);
    assert(l->next == NULL);

    cangjie_char_list_free(l);
    cangjie_free(cj);
}

static void test_cangjie_get_characters__results_order(void) {
    Cangjie *cj;
    CangjieCharList *l;
    const CangjieCharList *cur;

    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters(cj, "h*i", &l);
    assert(ret == CANGJIE_OK);

    cur = l;
    assert(strcmp(cur->c->chchar, "\xE5\x87\xA1") == 0);  // 凡
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE4\xB9\x88") == 0);  // 么
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE4\xB8\x9F") == 0);  // 丟
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE5\xA4\x99") == 0);  // 夙
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE8\x88\x9F") == 0);  // 舟
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE5\x8D\xB5") == 0);  // 卵
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE6\x88\x91") == 0);  // 我
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE7\xA7\x81") == 0);  // 私
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE7\x9A\x84") == 0);  // 的
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE5\xBE\x85") == 0);  // 待
    assert(cur->next != NULL);

    cangjie_char_list_free(l);
    cangjie_free(cj);
}

static void test_cangjie_get_codes_by_character(void) {
    Cangjie *cj;
    CangjieCharList *l;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    // Use common character to test the function
    // 日 (U+65E5), known to have 1 code in libcangjie, to test
    ret = cangjie_get_codes_by_character(cj, "\xE6\x97\xA5", &l);
    assert(ret == CANGJIE_OK);

    assert(strcmp(l->c->chchar, "\xE6\x97\xA5") == 0);  // 日
    assert(strcmp(l->c->code, "a") == 0);
    assert(l->next == NULL);

    cangjie_char_list_free(l);

    // Use an obscure cantonese character with known multiple mapping
    // 乖 (U+4E56), known to have 2 codes in libcangjie, to test
    // the function
    ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    ret = cangjie_get_codes_by_character(cj, "\xE4\xB9\x96", &l);
    assert(ret == CANGJIE_OK);

    assert(strcmp(l->c->chchar, "\xE4\xB9\x96") == 0);  // 乖
    assert(strcmp(l->c->code, "hjlmp") == 0);
    assert(l->next != NULL);

    assert(strcmp(l->next->c->chchar, "\xE4\xB9\x96") == 0);  // 乖
    assert(strcmp(l->next->c->code, "hjlp") == 0);
    // higher frequency code comes first
    assert(l->c->frequency > l->next->c->frequency);
    assert(l->next->next == NULL);

    cangjie_char_list_free(l);

    // Use a "character" string that is not Chinese to test the function
    // "abc" is not a valid Chinese character, so the function should
    // return CANGJIE_NOCHARS
    ret = cangjie_get_codes_by_character(cj, "abc", &l);
    assert(ret == CANGJIE_NOCHARS);

    cangjie_free(cj);
}

static void test_cangjie_get_characters_by_shortcode(void) {
    Cangjie *cj;
    CangjieCharList *l;
    CangjieCharList *cur;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters_by_shortcode(cj, ",", &l);
    assert(ret == CANGJIE_OK);

    cur = l;
    assert(strcmp(cur->c->chchar, "\xE3\x80\x81") == 0);  // 、
    assert(strcmp(cur->c->code, ",") == 0);
    assert(cur->c->frequency == 1);
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xEF\xBC\x8C") == 0);  // ，
    assert(strcmp(cur->c->code, ",") == 0);
    assert(cur->c->frequency == 0);
    assert(cur->next == NULL);

    cangjie_char_list_free(l);
    cangjie_free(cj);
}

static void test_cangjie_get_characters__multiple_queries(void) {
    Cangjie *cj;
    CangjieCharList *l;
    CangjieCharList *l2;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters(cj, "h*i", &l);
    assert(ret == CANGJIE_OK);
    cangjie_char_list_free(l);

    ret = cangjie_get_characters(cj, "dmlm", &l2);
    assert(ret == CANGJIE_OK);

    assert(strcmp(l2->c->chchar, "\xE6\xA4\x8F") == 0);  // 椏
    assert(strcmp(l2->c->code, "dmlm") == 0);
    assert(l2->c->frequency == 10253);
    assert(l2->next == NULL);

    cangjie_char_list_free(l2);
    cangjie_free(cj);
}

static void test_cangjie_get_characters__with_wildcards(void) {
    Cangjie *cj;
    CangjieCharList *l;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_5, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_5);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters(cj, "dd*", &l);
    assert(ret == CANGJIE_INVALID);
}

static void test_cangjie_get_characters_v2__single_result(void) {
    Cangjie *cj;
    CangjieCharList *l;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters_v2(cj, "dmlm", &l);
    assert(ret == CANGJIE_OK);

    assert(strcmp(l->c->chchar, "\xE6\xA4\x8F") == 0);  // 椏
    assert(strcmp(l->c->code, "dmlm") == 0);
    assert(l->c->frequency == 10253);
    assert(l->next == NULL);

    cangjie_char_list_free(l);
    cangjie_free(cj);
}

static void test_cangjie_get_characters_v2__results_order(void) {
    Cangjie *cj;
    CangjieCharList *l;
    const CangjieCharList *cur;

    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters_v2(cj, "h*i", &l);
    assert(ret == CANGJIE_OK);

    cur = l;
    assert(strcmp(cur->c->chchar, "\xE5\x87\xA1") == 0);  // 凡
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE4\xB9\x88") == 0);  // 么
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE4\xB8\x9F") == 0);  // 丟
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE5\xA4\x99") == 0);  // 夙
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE8\x88\x9F") == 0);  // 舟
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE5\x8D\xB5") == 0);  // 卵
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE6\x88\x91") == 0);  // 我
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE7\xA7\x81") == 0);  // 私
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE7\x9A\x84") == 0);  // 的
    assert(cur->next != NULL);

    cur = cur->next;
    assert(strcmp(cur->c->chchar, "\xE5\xBE\x85") == 0);  // 待
    assert(cur->next != NULL);

    cangjie_char_list_free(l);
    cangjie_free(cj);
}

static void test_cangjie_get_characters_v2__multiple_queries(void) {
    Cangjie *cj;
    CangjieCharList *l;
    CangjieCharList *l2;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters_v2(cj, "h*i", &l);
    assert(ret == CANGJIE_OK);
    cangjie_char_list_free(l);

    ret = cangjie_get_characters_v2(cj, "dmlm", &l2);
    assert(ret == CANGJIE_OK);

    assert(strcmp(l2->c->chchar, "\xE6\xA4\x8F") == 0);  // 椏
    assert(strcmp(l2->c->code, "dmlm") == 0);
    assert(l2->c->frequency == 10253);
    assert(l2->next == NULL);

    cangjie_char_list_free(l2);
    cangjie_free(cj);
}

static void test_cangjie_get_characters_v2__with_wildcards(void) {
    Cangjie *cj;
    CangjieCharList *l;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_5, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_5);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_characters_v2(cj, "dd*", &l);
    assert(ret == CANGJIE_OK);
    assert(strcmp(l->next->c->chchar, "\xe6\x9d\x91") == 0);
}

static void test_cangjie_get_radical(void) {
    Cangjie *cj;
    char *radical;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    ret = cangjie_get_radical(cj, 'a', &radical);
    assert(ret == CANGJIE_OK);
    assert(strcmp(radical, "\xE6\x97\xA5") == 0);  // 日

    ret = cangjie_get_radical(cj, 'h', &radical);
    assert(ret == CANGJIE_OK);
    assert(strcmp(radical, "\xE7\xAB\xB9") == 0);  // 竹

    ret = cangjie_get_radical(cj, '*', &radical);
    assert(ret == CANGJIE_OK);
    assert(strcmp(radical, "\xEF\xBC\x8A") == 0);  // ＊

    ret = cangjie_get_radical(cj, 'z', &radical);
    assert(ret == CANGJIE_OK);
    assert(strcmp(radical, "\xEF\xBC\xBA") == 0);  // Ｚ

    ret = cangjie_get_radical(cj, 'x', &radical);
    assert(ret == CANGJIE_OK);
    assert(strcmp(radical, "\xE9\x9B\xA3") == 0);  // 難

    cangjie_free(cj);
}

static void test_cangjie_get_radicals(void) {
    Cangjie *cj;
    char *radicals;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    ret = cangjie_get_radicals(cj, "ab", &radicals);
    assert(ret == CANGJIE_OK);
    assert(strcmp(radicals, "\xE6\x97\xA5\xE6\x9C\x88") == 0);  // 日月
    free(radicals);

    ret = cangjie_get_radicals(cj, "abue", &radicals);
    assert(ret == CANGJIE_OK);
    assert(
        strcmp(radicals, "\xE6\x97\xA5\xE6\x9C\x88\xE5\xB1\xB1\xE6\xB0\xB4") ==
        0);  // 日月山水
    free(radicals);

    ret = cangjie_get_radicals(cj, "h*", &radicals);
    assert(ret == CANGJIE_OK);
    assert(strcmp(radicals, "\xE7\xAB\xB9\xEF\xBC\x8A") == 0);  // 竹＊
    free(radicals);

    ret = cangjie_get_radicals(cj, "z;", &radicals);
    assert(ret == CANGJIE_INVALID);
}

static void test_cangjie_is_input_key(void) {
    Cangjie *cj;
    int ret = cangjie_new(&cj, CANGJIE_VERSION_3, CANGJIE_FILTER_BIG5);
    assert(ret == CANGJIE_OK);

    assert(cj->version == CANGJIE_VERSION_3);
    assert(cj->filter_flags == CANGJIE_FILTER_BIG5);

    assert(cangjie_is_input_key(cj, 'a') == CANGJIE_OK);
    assert(cangjie_is_input_key(cj, 'z') == CANGJIE_OK);
    assert(cangjie_is_input_key(cj, 'A') == CANGJIE_INVALID);
    assert(cangjie_is_input_key(cj, 'Z') == CANGJIE_INVALID);
    assert(cangjie_is_input_key(cj, '0') == CANGJIE_INVALID);
    assert(cangjie_is_input_key(cj, '9') == CANGJIE_INVALID);
    assert(cangjie_is_input_key(cj, '*') == CANGJIE_INVALID);

    cangjie_free(cj);
}

int main(int argc, char *argv[]) {
    int opt;
    int verbose = 0;
    const char *database_path;

    // Parse command options with getopt_long for -v/--verbose flag
    static struct option long_options[] = {{"verbose", no_argument, 0, 'v'},
                                           {0, 0, 0, 0}};

    while ((opt = getopt_long(argc, argv, "v", long_options, NULL)) != -1) {
        switch (opt) {
            case 'v':
                verbose = 1;
                break;
            default:
                fprintf(stderr, "Usage: %s [-v|--verbose]\n", argv[0]);
                exit(1);
        }
    }

    // Set the CANGJIE_DB environment variable
    // to default installation location if it is not
    // set in the environment.
    database_path = getenv("CANGJIE_DB");
    if (database_path == NULL) {
        set_env_var("CANGJIE_DB", CANGJIE_DB);
    }
    if (verbose) {
        printf("CANGJIE_DB used for test_cangjie: %s\n", getenv("CANGJIE_DB"));
    }

    test_cangjie_new();
    test_cangjie_new__with_all_filters();
    test_cangjie_new__without_big5();
    test_cangjie_get_characters__single_result();
    test_cangjie_get_characters__results_order();
    test_cangjie_get_characters__multiple_queries();
    test_cangjie_get_characters__with_wildcards();
    test_cangjie_get_characters_v2__single_result();
    test_cangjie_get_characters_v2__results_order();
    test_cangjie_get_characters_v2__multiple_queries();
    test_cangjie_get_characters_v2__with_wildcards();
    test_cangjie_get_codes_by_character();
    test_cangjie_get_characters_by_shortcode();
    test_cangjie_get_radical();
    test_cangjie_get_radicals();
    test_cangjie_is_input_key();
    return 0;
}
