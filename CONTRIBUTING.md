# Contributing

Thanks for your interest in contributing to libcangjie. :grin:
Free software can only thrive thanks to people like you who take the
time to help.

The very first thing you need to do is accept our [code of conduct][coc]
(we took it from [Freedesktop.org][freedesktop-coc]).

[freedesktop-coc]: https://www.freedesktop.org/wiki/CodeOfConduct/
[coc]: code-of-conduct.md

<details>
<summary>Table of Contents</summary>

- [Contribution Guidelines](#contribution-guidelines)
  - [Filing issues](#filing-issues)
  - [Submitting changes](#submitting-changes)
    - [Code Formatting](#code-formatting)
      - [cppcheck](#cppcheck)
      - [clang-format](#clang-format)
    - [Commit history](#commit-history)
    - [Commit messages](#commit-messages)
- [Working with Source Code](#working-with-source-code)
  - [Build and Install](#build-and-install)
  - [After Installation](#after-installation)
  - [Unit Test](#unit-test)
- [Distribution](#distribution)
</details>

## Contribution Guidelines

Following these guidelines helps to communicate that you respect the time of
the developers managing and developing this project. In return, they should
reciprocate that respect in addressing your issue, assessing changes, and
helping you finalize your contributions.

### Filing issues

Whenever you experience a problem with this software, please let us know so we
can make it better.

But first, take the time to search through the list of [existing issues] to see
whether it is already known.

Be sure to provide all the information you can, as that will really help us
fixing your issue as quickly as possible.

[existing issues]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/issues

### Submitting changes

If you want to implement a new feature, make sure you
[first open an issue][creating issue] to discuss it and ensure the feature
is desired.

We accept changes through the usual merge request process.

[creating issue]: https://gitlab.freedesktop.org/cangjie/libcangjie/-/issues/new

#### Code Formatting

To keep the quality and consistancy of our source code, we use:

- [cppcheck][cppcheck] to check for potential bugs.
- [clang-format][clang-format] to ensure consistancy of code format.

After finishing your feature, we recommend you to run both against your source
code.

[clang-format]: https://clang.llvm.org/docs/ClangFormat.html
[cppcheck]: https://cppcheck.net/

##### cppcheck

`cppcheck` can point out the part of your code that either have potential
problems, or can be improved.

Both Fedora/RHEL and Debian/Ubuntu have package named `cppcheck`. You may
install it easily.

To run cppcheck, you should build a tentative source code and then test
against it:

```bash
meson setup ./builddir --prefix /usr --wipe
ninja -C ./builddir cppcheck
```

You're advised to change your source code to the recommendations of it.

Alternatively, major editors like [Atom][atom-cppcheck],
[Sublime][sublime-cppcheck], [vim][vim-cppcheck], and [VSCode][vscode-cppcheck]
all have linter based on `cppcheck`. It would be helpful if you run them in
your editor.

[vscode-cppcheck]: https://marketplace.visualstudio.com/items?itemName=NathanJ.cppcheck-plugin
[atom-cppcheck]: https://github.com/asmatic77/linter-cppcheck
[sublime-cppcheck]: https://github.com/SublimeLinter/SublimeLinter-cppcheck
[vim-cppcheck]: https://github.com/vim-scripts/cpp_cppcheck.vim

##### clang-format

`clang-format` can directly reformat your code based on the
[.clang-format](.clang-format) config file in our repository.

`clang-format` is included in Fedora/RHEL by the package `clang-tools-extra`.
It is packaged in Debian/Ubuntu in the package `clang-format`. Please install
the package.

To run `clang-format` in in-place edit mode, simply run it in the repository
root folder:

```
clang-format -i [PATH_TO_SOURCE_FILE1] [PATH_TO_SOURCE_FILE1] ...
```

Or, if you're not at the root folder:

```
clang-format -i --style=file:[PATH_TO_DOT_CLANG_FORMAT] [PATH_TO_SOURCE_FILE1] [PATH_TO_SOURCE_FILE2] ...
```

Alternatively, major editors like [Atom][atom-clang-format],
[Sublime][sublime-clang-format], [vim][vim-clang-format], and
[VSCode][vscode-clang-format] all have plugins for clang-format. Some even
support formatting-as-your-save, which saves you the trouble to run the tool
manually.

[atom-clang-format]: https://github.com/andrew-d-jackson/atom-clang-format
[sublime-clang-format]: https://packagecontrol.io/packages/Clang%20Format
[vim-clang-format]: https://github.com/rhysd/vim-clang-format
[vscode-clang-format]: https://marketplace.visualstudio.com/items?itemName=xaver.clang-format



#### Commit history

Try to keep your branch split into atomic commits.

For example, if you made a first commit to implement a change, then a second
one to fix a problem in the first commit, squash them together.

If you need help doing that, please mention it in your merge request and we
will guide you towards using Git to achieve a nice and clean branch history. :smile:

#### Commit messages

Commit messages are extremely important. They tell the story of the project,
how it arrived to where it is now, why we took the decisions that made it the
way it is.

Chris Beams wrote [a wonderful post](https://chris.beams.io/posts/git-commit/)
that we highly recommend you read. The highlight of the post are 7 rules we ask
you to try and follow:

1.  Separate subject from body with a blank line
2.  Limit the subject line to 50 characters
3.  Capitalize the subject line
4.  Do not end the subject line with a period
5.  Use the imperative mood in the subject line
6.  Wrap the body at 72 characters
7.  Use the body to explain what and why vs. how

We further want to add our own rules:

*   if the commit is related to an issue, please mention the id of that issue
    in the commit message; for example, use something like `Fixes #123` or
    `Implements #123`.


## Working with Source Code

If you want to work on the source code, this is a short guideline for it:

Before working on the source code, please make sure the following
dependencies are installed:

* [gcc][gcc]
* [pkgconf][pkgconf]
* [sqlite][sqlite] along with development header files
* [meson][meson]

[gcc]: https://gcc.gnu.org/
[sqlite]: https://www.sqlite.org/index.html
[pkgconf]: https://github.com/pkgconf/pkgconf
[meson]: https://mesonbuild.com/

You can get the latest sources from our Git repository:

```bash
git clone https://gitlab.freedesktop.org/cangjie/libcangjie.git
```

### Build and Install

To cleanly setup the project for build with meson:

```bash
meson setup --prefix=/usr --wipe ./builddir
```

To generate the resources out of the sources for this setup:

```bash
meson compile -C ./builddir
```

To install _(**Caution: This will overwrite all existing package-installed files**)_:

```bash
meson install -C ./builddir
```

Supported platforms are Fedora and Ubuntu, that's what we test in our CI. If
the tests fail on another platform then do let us know and we'll see whether we
can add it to our CI so it gets tested as well. :smile:

You can also generate your own tarball:

```bash
meson dist -C builddir
```

### After Installation

A CLI tool `libcangjie-cli` is included for checking the functionality of the
library. You are welcome to run and test the library's feature:

```bash
libcangjie-cli --help
```

### Unit Test

We have a few unit test binary in the tests folder. To run them against the
latest source code:

```bash
meson setup ./builddir --prefix /usr --wipe
meson test -C ./builddir
```

## Distribution

We are eager to expand our support to different distributions (see
[this page][install] for currently supported distros). But we are a very small
team. Please understand that we do not use all distros day-to-day.

If you have experience packaging or even distributing to new Linux distros,
please [create a new issue][creating issue]. We can provide any help that
you need. And we would love to update [our website][website] for a new
installation instruction.

[install]: https://cangjie.pages.freedesktop.org/projects/libcangjie/install.html
[website]: https://cangjie.pages.freedesktop.org/
