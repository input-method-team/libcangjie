/* Copyright (c) 2013 - The libcangjie authors.
 *
 * This file is part of libcangjie.
 *
 * libcangjie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libcangjie is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libcangjie.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cangjie.h>
#include <getopt.h>
#include <libcangjie.h>
#include <libgen.h>
#include <stdio.h>
#include <string.h>

typedef enum CangjieCliMode {
    CANGJIE_CLI_MODE_CODE = 0,
    CANGJIE_CLI_MODE_SHORTCODE = 1,
    CANGJIE_CLI_MODE_RADICAL = 2,
    CANGJIE_CLI_MODE_WILDCARDS = 3,
} CangjieCliMode;

/**
 * @brief Print overall help message
 * @param progname
 */
static void print_help(char *progname) {
    printf("Usage: %s [COMMAND] [OPTIONS]... [ARGS]...\n", basename(progname));
    printf(
        "A CLI interface of libcangjie cangjie code query\n\n"
        "Commands:\n"
        "  help     Print help message about the 2 subcommands\n"
        "  version  Show libcangjie version information\n"
        "  query    Perform code query\n"
        "\nFor complete documentation, please go to our website: \n"
        "<https://cangjie.pages.freedesktop.org/>\n");
}

/**
 * @brief Print help message for subcommand "query"
 * @param progname
 */
static void print_query_help(char *progname, const char *subcommand) {
    printf("Usage: %s %s [OPTIONS]... CODE\n", basename(progname), subcommand);
    printf(
        "A CLI interface of libcangjie cangjie code query\n\n"
        "-f, --filter=FILTER1,FILTER2...  specify the filters used in the "
        "query\n"
        "                                 default: big5,hkscs\n"
        "                                 acceptable values:\n"
        "                                   big5, hkscs, punctuation, "
        "chinese,\n"
        "                                   zhuyin, kanji, katakana, "
        "hiragana,\n"
        "                                   symbols\n"
        "-m, --mode=MODE                  specify the mode of query\n"
        "                                 default: code\n"
        "                                 acceptable values:\n"
        "                                   code, wildcards, shortcode, "
        "radical\n"
        "-C, --cj-version=VERSION         specify the version of Cangjie used\n"
        "                                 default: 3\n"
        "                                 acceptable values: 3, 5\n"
        "-h, --help                       print this help message and leave\n"
        "\nFor complete documentation, please go to our website: \n"
        "<https://cangjie.pages.freedesktop.org/>\n");
}

/**
 * @brief Print overall help message
 * @param progname Program name
 * @param subcommand Subcommand name
 * @param msg Error message
 */
static void print_subcommand_error(char *progname, const char *subcommand,
                                   const char *msg) {
    printf("%s %s: %s\n", basename(progname), subcommand, msg);
    printf("Try '%s %s --help' for more information\n", basename(progname),
           subcommand);
}

/**
 * @brief Handler of subcommand "version"
 * @param argc Number of arguments passed to the subcommand
 * @param argv Arguments passed to the subcommand
 * @return 0
 */
static int subcommand_version(const char *progname, int argc, char **argv) {
    printf("libcangjie version %s\n", libcangjie_version());
    return 0;
}

/**
 * @brief Handler of subcommand "query"
 * @param argc Number of arguments passed to the subcommand
 * @param argv Arguments passed to the subcommand
 * @return Return 0 for success; or -1 for failure
 */
static int subcommand_query(char *progname, int argc, char **argv) {
    char *code;
    Cangjie *cj;
    int ret;
    CangjieCharList *chars;
    const CangjieCharList *iter;

    // variables to parse options
    int option_index;

    // option flags
    int opt_filter;
    CangjieCliMode opt_mode;
    int opt_cj_version;

    // helper variable(s)
    char errmsg[255];
    char *ptr_radical;

    // available options
    static struct option long_options[] = {
        {"filter", required_argument, 0, 'f'},
        {"mode", required_argument, 0, 'm'},
        {"cj-version", required_argument, 0, 'C'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0},
    };

    // default options
    opt_filter = CANGJIE_FILTER_BIG5 | CANGJIE_FILTER_HKSCS;
    opt_mode = CANGJIE_CLI_MODE_CODE;
    opt_cj_version = CANGJIE_VERSION_3;

    // read options
    while (1) {
        int opt;
        option_index = 0;
        opt = getopt_long(argc, argv, "f:m:C:Vh", long_options, &option_index);

        if (opt == -1) {
            break;
        }

        switch (opt) {
            case 'f':
                opt_filter = 0;
                if (strstr(optarg, "big5") != NULL) {
                    opt_filter = CANGJIE_FILTER_BIG5;
                }
                if (strstr(optarg, "hkscs") != NULL) {
                    opt_filter = opt_filter | CANGJIE_FILTER_HKSCS;
                }
                if (strstr(optarg, "punctuation") != NULL) {
                    opt_filter = opt_filter | CANGJIE_FILTER_PUNCTUATION;
                }
                if (strstr(optarg, "chinese") != NULL) {
                    opt_filter = opt_filter | CANGJIE_FILTER_CHINESE;
                }
                if (strstr(optarg, "zhuyin") != NULL) {
                    opt_filter = opt_filter | CANGJIE_FILTER_ZHUYIN;
                }
                if (strstr(optarg, "kanji") != NULL) {
                    opt_filter = opt_filter | CANGJIE_FILTER_KANJI;
                }
                if (strstr(optarg, "katakana") != NULL) {
                    opt_filter = opt_filter | CANGJIE_FILTER_KATAKANA;
                }
                if (strstr(optarg, "hiragana") != NULL) {
                    opt_filter = opt_filter | CANGJIE_FILTER_HIRAGANA;
                }
                if (strstr(optarg, "symbols") != NULL) {
                    opt_filter = opt_filter | CANGJIE_FILTER_SYMBOLS;
                }
                if (opt_filter == 0) {
                    sprintf(errmsg, "Invalid filter option '%s'", optarg);
                    print_subcommand_error(progname, argv[0], errmsg);
                    return -1;
                }
                break;
            case 'm':
                if (strcmp(optarg, "code") == 0) {
                    opt_mode = CANGJIE_CLI_MODE_CODE;
                } else if (strcmp(optarg, "wildcards") == 0) {
                    opt_mode = CANGJIE_CLI_MODE_WILDCARDS;
                } else if (strcmp(optarg, "shortcode") == 0) {
                    opt_mode = CANGJIE_CLI_MODE_SHORTCODE;
                } else if (strcmp(optarg, "radical") == 0) {
                    opt_mode = CANGJIE_CLI_MODE_RADICAL;
                } else {
                    sprintf(errmsg, "Invalid query mode '%s'", optarg);
                    print_subcommand_error(progname, argv[0], errmsg);
                    return -1;
                }
                break;
            case 'C':
                if (*optarg == '3') {
                    opt_cj_version = CANGJIE_VERSION_3;
                } else if (*optarg == '5') {
                    opt_cj_version = CANGJIE_VERSION_5;
                } else {
                    sprintf(errmsg, "Invalid Cangjie version '%s'", optarg);
                    print_subcommand_error(progname, argv[0], errmsg);
                    return -1;
                }
                break;
            case '?':
                printf("Try '%s %s --help' for more information\n",
                       basename(progname), argv[0]);
                return -1;
                break;
            case 'h':
            default:
                print_query_help(progname, argv[0]);
                return (opt == 'h') ? 0 : -1;
        }
    }

    // check if query provided
    if (optind == argc) {
        print_subcommand_error(progname, argv[0], "missing query code");
        return -1;
    }

    code = argv[optind];

    ret = cangjie_new(&cj, opt_cj_version, opt_filter);

    if (ret == CANGJIE_DBOPEN) {
        printf("Could not open the Cangjie database\n");
        return ret;
    } else if (ret != CANGJIE_OK) {
        printf("Unhandled error while creating the Cangjie object: %d\n", ret);
        return ret;
    }

    if (opt_mode == CANGJIE_CLI_MODE_RADICAL) {
        char *code_ptr;
        code_ptr = code;
        for (; *code_ptr != '\0'; code_ptr++) {
            ret = cangjie_get_radical(cj, *code_ptr, &ptr_radical);
            if (ret == CANGJIE_OK) {
                printf("code: '%c', radical: '%s'\n", *code_ptr, ptr_radical);
            } else {
                printf("Query error (%d)\n", ret);
                return ret;
            }
        }
        return ret;
    } else {
        if (opt_mode == CANGJIE_CLI_MODE_CODE) {
            ret = cangjie_get_characters(cj, code, &chars);
        } else if (opt_mode == CANGJIE_CLI_MODE_WILDCARDS) {
            ret = cangjie_get_characters_v2(cj, code, &chars);
        } else if (opt_mode == CANGJIE_CLI_MODE_SHORTCODE) {
            ret = cangjie_get_characters_by_shortcode(cj, code, &chars);
        }

        if (ret == CANGJIE_NOCHARS) {
            printf("No chars with code '%s'\n", code);

            cangjie_free(cj);
            return CANGJIE_NOCHARS;
        } else if (ret == CANGJIE_INVALID) {
            printf("Invalid code '%s'\n", code);

            cangjie_free(cj);
            return CANGJIE_INVALID;
        } else if (ret != CANGJIE_OK) {
            printf("Other error %d with code '%s'\n", ret, code);

            cangjie_free(cj);
            return ret;
        }

        iter = chars;

        while (1) {
            const CangjieChar *c;

            if (iter == NULL) {
                break;
            }

            c = iter->c;
            printf(
                "Char: '%s', SimpChar: '%s', code: '%s', "
                "classic frequency: %d\n",
                c->chchar, c->simpchar, c->code, c->frequency);

            iter = iter->next;
        }

        cangjie_char_list_free(chars);
    }

    cangjie_free(cj);

    return CANGJIE_OK;
}

int main(int argc, char **argv) {
    const char *subcommand;

    if (argc < 2) {
        printf("Missing subcommand\n");
        print_help(argv[0]);
        return -1;
    }

    subcommand = argv[1];

    if (strcmp(subcommand, "help") == 0) {
        print_help(argv[0]);
        return 0;
    } else if (strcmp(subcommand, "version") == 0) {
        return subcommand_version(argv[0], argc - 1, argv + 1);
    } else if (strcmp(subcommand, "query") == 0) {
        return subcommand_query(argv[0], argc - 1, argv + 1);
    } else {
        printf("Invalid subcommand: %s\n", subcommand);
        print_help(argv[0]);
        return -1;
    }

    return 0;
}
