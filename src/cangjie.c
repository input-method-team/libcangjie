﻿/* Copyright (c) 2013 - The libcangjie authors.
 *
 * This file is part of libcangjie.
 *
 * libcangjie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libcangjie is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libcangjie.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cangjie.h"

#include <stdlib.h>
#include <string.h>

#include "config.h"

// Longest possible filter query has a length of 127:
//     " AND ( big5 = 1 OR hkscs = 1 OR punct = 1 OR ... ) "
#define MAX_LEN_FILTER_QUERY 106

const char *cangjie_radicals[] = {
    "\xE6\x97\xA5",  // 日
    "\xE6\x9C\x88",  // 月
    "\xE9\x87\x91",  // 金
    "\xE6\x9C\xA8",  // 木
    "\xE6\xB0\xB4",  // 水
    "\xE7\x81\xAB",  // 火
    "\xE5\x9C\x9F",  // 土
    "\xE7\xAB\xB9",  // 竹
    "\xE6\x88\x88",  // 戈
    "\xE5\x8D\x81",  // 十
    "\xE5\xA4\xA7",  // 大
    "\xE4\xB8\xAD",  // 中
    "\xE4\xB8\x80",  // 一
    "\xE5\xBC\x93",  // 弓
    "\xE4\xBA\xBA",  // 人
    "\xE5\xBF\x83",  // 心
    "\xE6\x89\x8B",  // 手
    "\xE5\x8F\xA3",  // 口
    "\xE5\xB0\xB8",  // 尸
    "\xE5\xBB\xBF",  // 廿
    "\xE5\xB1\xB1",  // 山
    "\xE5\xA5\xB3",  // 女
    "\xE7\x94\xB0",  // 田
    "\xE9\x9B\xA3",  // 難
    "\xE5\x8D\x9C",  // 卜
    "\xEF\xBC\xBA",  // Ｚ
};

static void strlcat_or_operator(uint32_t *first, char *query) {
    if (!*first) {
        strlcat(query, " OR ", strlen(query) + 5);
    } else {
        *first = 0;
    }
}

static int cangjie_get_filter_query(const Cangjie *cj, char **query) {
    uint32_t first = 1;
    if (cj->filter_flags == 0) {
        // No filter means pass all, so let's return an empty string
        *query = calloc(1, sizeof(char));
        if (*query == NULL) {
            return CANGJIE_NOMEM;
        }

        return CANGJIE_OK;
    }

    *query = calloc(MAX_LEN_FILTER_QUERY + 1, sizeof(char));
    if (*query == NULL) {
        return CANGJIE_NOMEM;
    }

    strlcat(*query, "AND (", strlen(*query) + 6);

    if (cj->filter_flags & CANGJIE_FILTER_BIG5) {
        strlcat(*query, "big5=1", strlen(*query) + 7);
        first = 0;
    }

    if (cj->filter_flags & CANGJIE_FILTER_HKSCS) {
        strlcat_or_operator(&first, *query);
        strlcat(*query, "hkscs=1", strlen(*query) + 8);
    }

    if (cj->filter_flags & CANGJIE_FILTER_PUNCTUATION) {
        strlcat_or_operator(&first, *query);
        strlcat(*query, "punct=1", strlen(*query) + 8);
    }

    if (cj->filter_flags & CANGJIE_FILTER_CHINESE) {
        strlcat_or_operator(&first, *query);
        strlcat(*query, "zh=1", strlen(*query) + 5);
    }

    if (cj->filter_flags & CANGJIE_FILTER_ZHUYIN) {
        strlcat_or_operator(&first, *query);
        strlcat(*query, "zhuyin=1", strlen(*query) + 9);
    }

    if (cj->filter_flags & CANGJIE_FILTER_KANJI) {
        strlcat_or_operator(&first, *query);
        strlcat(*query, "kanji=1", strlen(*query) + 8);
    }

    if (cj->filter_flags & CANGJIE_FILTER_KATAKANA) {
        strlcat_or_operator(&first, *query);
        strlcat(*query, "katakana=1", strlen(*query) + 11);
    }

    if (cj->filter_flags & CANGJIE_FILTER_HIRAGANA) {
        strlcat_or_operator(&first, *query);
        strlcat(*query, "hiragana=1", strlen(*query) + 11);
    }

    if (cj->filter_flags & CANGJIE_FILTER_SYMBOLS) {
        strlcat_or_operator(&first, *query);
        strlcat(*query, "symbol=1", strlen(*query) + 9);
    }

    strlcat(*query, ") ", strlen(*query) + 3);

    return CANGJIE_OK;
}

static int cangjie_build_query(Cangjie **cj, char **query,
                               const char *extra_conditions) {
    char *filter_query;
    int extra_len;
    int ret;

    const char *base_query =
        "SELECT chchar, simpchar, code, frequency\n"
        "FROM chars\n"
        "INNER JOIN codes on chars.char_index=codes.char_index\n"
        "WHERE version=? ";

    // Since the results will be added to a linked list by "prepend",
    // the order of results would be the reverse order of this query.
    // Hence we use ASC in the order-by-clause to get a DESC order
    // in the actual results.
    const char *order_by_clause = "ORDER BY frequency ASC";

    extra_len = (extra_conditions != NULL && strlen(extra_conditions) > 0)
                    ? strlen(extra_conditions) + strlen("AND ")
                    : 0;

    *query = calloc(strlen(base_query) + MAX_LEN_FILTER_QUERY +
                        strlen(order_by_clause) + extra_len + 1,
                    sizeof(char));
    if (*query == NULL) {
        return CANGJIE_NOMEM;
    }

    strlcat(*query, base_query, strlen(*query) + strlen(base_query) + 1);
    ret = cangjie_get_filter_query(*cj, &filter_query);
    if (ret != CANGJIE_OK) {
        free(*query);
        free(filter_query);
        *query = NULL;
        return ret;
    }

    strlcat(*query, filter_query, strlen(*query) + strlen(filter_query) + 1);
    if (extra_conditions != NULL && strlen(extra_conditions) > 0) {
        strlcat(*query, "AND ", strlen(*query) + 5);
        strlcat(*query, extra_conditions,
                strlen(*query) + strlen(extra_conditions) + 1);
    }
    strlcat(*query, order_by_clause,
            strlen(*query) + strlen(order_by_clause) + 1);

    free(filter_query);
    return CANGJIE_OK;
}

int cangjie_new(Cangjie **cj, CangjieVersion version,
                unsigned int filter_flags) {
    int ret;
    char *database_path;
    Cangjie *tmp = calloc(1, sizeof(Cangjie));
    if (tmp == NULL) {
        return CANGJIE_NOMEM;
    }

    tmp->version = version;
    tmp->filter_flags = filter_flags;

    // Prepare the main Cangjie query
    ret = cangjie_build_query(&tmp, &tmp->cj_query, "code = ? ");
    if (ret != CANGJIE_OK) {
        cangjie_free(tmp);
        return ret;
    }
    ret = cangjie_build_query(&tmp, &tmp->cj_query_wildcard, "code GLOB ? ");
    if (ret != CANGJIE_OK) {
        cangjie_free(tmp);
        return ret;
    }

    // Check the CANGJIE_DB env var (it is useful for local testing)
    database_path = getenv("CANGJIE_DB");
    if (database_path != NULL) {
        ret = sqlite3_open_v2(database_path, &tmp->db, SQLITE_OPEN_READONLY,
                              NULL);
    } else {
        ret = sqlite3_open_v2(CANGJIE_DB, &tmp->db, SQLITE_OPEN_READONLY, NULL);
    }
    if (ret == SQLITE_CANTOPEN) {
        cangjie_free(tmp);
        return CANGJIE_DBOPEN;
    } else if (ret != SQLITE_OK) {
        cangjie_free(tmp);
        // FIXME: Unhandled error codes
        return ret;
    }

    *cj = tmp;

    return CANGJIE_OK;
}

/**
 * Searching for characters with confined wildcard support.
 *
 * This function supported limited use of wildcard character '*'. Wildcard
 * character '*' can only be used in the middle of the input_code.
 *
 * @param cj The Cangjie object.
 * @param input_code The input code to search for.
 * @param l The resulting list of characters.
 *
 * @return CANGJIE_OK on success, or an error code on failure.
 */
int cangjie_get_characters(const Cangjie *cj, const char *input_code,
                           CangjieCharList **l) {
    CangjieCharList *tmp = NULL;
    sqlite3_stmt *stmt;
    const char *star_ptr;
    int ret;

    if (input_code == NULL || strlen(input_code) == 0 ||
        strlen(input_code) > 5) {
        return CANGJIE_INVALID;
    }

    if (input_code[0] == '*' || input_code[strlen(input_code) - 1] == '*') {
        return CANGJIE_INVALID;
    }

    // Determine if the input code contains a wildcard
    // and prepare stmt with the appropriate query string.
    star_ptr = strchr(input_code, '*');
    if (star_ptr == NULL) {
        ret = sqlite3_prepare_v2(cj->db, cj->cj_query, -1, &stmt, 0);
    } else {
        ret = sqlite3_prepare_v2(cj->db, cj->cj_query_wildcard, -1, &stmt, 0);
    }
    if (ret != SQLITE_OK) {
        return CANGJIE_DBERROR;
    }

    // Bind query parameters
    if (sqlite3_bind_int(stmt, 1, cj->version) != SQLITE_OK) {
        return CANGJIE_DBERROR;
    }
    if (sqlite3_bind_text(stmt, 2, input_code, -1, SQLITE_STATIC) !=
        SQLITE_OK) {
        return CANGJIE_DBERROR;
    }

    while (1) {
        ret = sqlite3_step(stmt);

        if (ret == SQLITE_ROW) {
            const char *chchar = (char *)sqlite3_column_text(stmt, 0);
            const char *simpchar = (char *)sqlite3_column_text(stmt, 1);
            const char *code = (char *)sqlite3_column_text(stmt, 2);
            uint32_t frequency = (uint32_t)sqlite3_column_int(stmt, 3);

            CangjieChar *c;
            ret = cangjie_char_new(&c, chchar, simpchar, code, frequency);
            if (ret != CANGJIE_OK) {
                return ret;
            }

            ret = cangjie_char_list_prepend(&tmp, c);
            if (ret != CANGJIE_OK) {
                return ret;
            }
        } else if (ret == SQLITE_DONE) {
            // All rows finished
            sqlite3_finalize(stmt);
            break;
        } else {
            // Some error encountered
            return CANGJIE_DBERROR;
        }
    }

    if (tmp == NULL) {
        return CANGJIE_NOCHARS;
    }

    *l = tmp;

    return CANGJIE_OK;
}

/**
 * Searching for characters with unconfined wildcard support.
 *
 * This function is a modified version of cangjie_get_characters that
 * supports the wildcard character '*' in the beginning or the end of
 * input_code. The only limitation is that input_code should not be
 * empty.
 *
 * @param cj The Cangjie object.
 * @param input_code The input code to search for.
 * @param l The resulting list of characters.
 *
 * @return CANGJIE_OK on success, or an error code on failure.
 */
int cangjie_get_characters_v2(const Cangjie *cj, const char *input_code,
                              CangjieCharList **l) {
    CangjieCharList *tmp = NULL;
    sqlite3_stmt *stmt;
    const char *star_ptr;
    int ret;

    if (input_code == NULL || strlen(input_code) == 0 ||
        strlen(input_code) > 5) {
        return CANGJIE_INVALID;
    }

    // Determine if the input code contains a wildcard
    // and prepare stmt with the appropriate query string.
    star_ptr = strchr(input_code, '*');
    if (star_ptr == NULL) {
        ret = sqlite3_prepare_v2(cj->db, cj->cj_query, -1, &stmt, 0);
    } else {
        ret = sqlite3_prepare_v2(cj->db, cj->cj_query_wildcard, -1, &stmt, 0);
    }
    if (ret != SQLITE_OK) {
        return CANGJIE_DBERROR;
    }

    // Bind query parameters
    if (sqlite3_bind_int(stmt, 1, cj->version) != SQLITE_OK) {
        return CANGJIE_DBERROR;
    }
    if (sqlite3_bind_text(stmt, 2, input_code, -1, SQLITE_STATIC) !=
        SQLITE_OK) {
        return CANGJIE_DBERROR;
    }

    while (1) {
        ret = sqlite3_step(stmt);

        if (ret == SQLITE_ROW) {
            const char *chchar = (char *)sqlite3_column_text(stmt, 0);
            const char *simpchar = (char *)sqlite3_column_text(stmt, 1);
            const char *code = (char *)sqlite3_column_text(stmt, 2);
            uint32_t frequency = (uint32_t)sqlite3_column_int(stmt, 3);

            CangjieChar *c;
            ret = cangjie_char_new(&c, chchar, simpchar, code, frequency);
            if (ret != CANGJIE_OK) {
                return ret;
            }

            ret = cangjie_char_list_prepend(&tmp, c);
            if (ret != CANGJIE_OK) {
                return ret;
            }
        } else if (ret == SQLITE_DONE) {
            // All rows finished
            sqlite3_finalize(stmt);
            break;
        } else {
            // Some error encountered
            return CANGJIE_DBERROR;
        }
    }

    if (tmp == NULL) {
        return CANGJIE_NOCHARS;
    }

    *l = tmp;

    return CANGJIE_OK;
}

int cangjie_get_codes_by_character(const Cangjie *cj, const char *chchar,
                                   CangjieCharList **l) {
    CangjieCharList *tmp = NULL;

    sqlite3_stmt *stmt;

    // Note: query by frequency in ascending order as we prepend to the list
    //       so the resulting order is descending by frequency.
    const char *sql =
        "SELECT chchar, simpchar, code, frequency\n"
        "FROM chars\n"
        "INNER JOIN codes on chars.char_index=codes.char_index\n"
        "WHERE version=? AND chchar=?\n"
        "ORDER BY frequency ASC";

    if (sqlite3_prepare_v3(cj->db, sql, -1, 0, &stmt, 0) != SQLITE_OK) {
        return CANGJIE_DBERROR;
    }
    if (sqlite3_bind_int(stmt, 1, cj->version) != SQLITE_OK) {
        return CANGJIE_DBERROR;
    }
    if (sqlite3_bind_text(stmt, 2, chchar, -1, SQLITE_STATIC) != SQLITE_OK) {
        return CANGJIE_DBERROR;
    }

    while (1) {
        int ret = sqlite3_step(stmt);

        if (ret == SQLITE_ROW) {
            const char *chchar_found = (char *)sqlite3_column_text(stmt, 0);
            const char *simpchar = (char *)sqlite3_column_text(stmt, 1);
            const char *code = (char *)sqlite3_column_text(stmt, 2);
            uint32_t frequency = (uint32_t)sqlite3_column_int(stmt, 3);

            CangjieChar *c;
            ret = cangjie_char_new(&c, chchar_found, simpchar, code, frequency);
            if (ret != CANGJIE_OK) {
                return ret;
            }

            ret = cangjie_char_list_prepend(&tmp, c);
            if (ret != CANGJIE_OK) {
                return ret;
            }
        } else if (ret == SQLITE_DONE) {
            // All rows finished
            sqlite3_finalize(stmt);
            break;
        } else {
            // Some error encountered
            return CANGJIE_DBERROR;
        }
    }

    if (tmp == NULL) {
        return CANGJIE_NOCHARS;
    }

    *l = tmp;

    return CANGJIE_OK;
}

int cangjie_get_characters_by_shortcode(const Cangjie *cj,
                                        const char *input_code,
                                        CangjieCharList **l) {
    CangjieCharList *tmp = NULL;

    sqlite3_stmt *stmt;
    const char *query =
        "SELECT chchar, simpchar, code, frequency\n"
        "FROM chars\n"
        "INNER JOIN codes on chars.char_index=codes.char_index\n"
        "WHERE version=0 AND code=?\n"
        "ORDER BY frequency ASC";

    if (input_code == NULL || strlen(input_code) != 1) {
        return CANGJIE_INVALID;
    }
    if (sqlite3_prepare_v2(cj->db, query, -1, &stmt, 0) != SQLITE_OK) {
        return CANGJIE_DBERROR;
    }
    if (sqlite3_bind_text(stmt, 1, input_code, -1, SQLITE_STATIC) !=
        SQLITE_OK) {
        return CANGJIE_DBERROR;
    }

    while (1) {
        int ret = sqlite3_step(stmt);

        if (ret == SQLITE_ROW) {
            const char *chchar = (char *)sqlite3_column_text(stmt, 0);
            const char *simpchar = (char *)sqlite3_column_text(stmt, 1);
            uint32_t frequency = (uint32_t)sqlite3_column_int(stmt, 3);

            CangjieChar *c;
            ret = cangjie_char_new(&c, chchar, simpchar, input_code, frequency);
            if (ret != CANGJIE_OK) {
                return ret;
            }

            ret = cangjie_char_list_prepend(&tmp, c);
            if (ret != CANGJIE_OK) {
                return ret;
            }
        } else if (ret == SQLITE_DONE) {
            // All rows finished
            sqlite3_finalize(stmt);
            break;
        } else {
            // Some error encountered
            return CANGJIE_DBERROR;
        }
    }

    if (tmp == NULL) {
        return CANGJIE_NOCHARS;
    }

    *l = tmp;

    return CANGJIE_OK;
}

int cangjie_get_radical(const Cangjie *cj, const char key, char **radical) {
    if ((key < 'a' || key > 'z') && (key != '*')) {
        return CANGJIE_INVALID;
    }

    if (key == '*') {
        // Special case for the wildcard '*'
        *radical = (char *)"＊";
    } else {
        // The actual Cangjie radicals
        *radical = (char *)cangjie_radicals[key - 'a'];
    }

    return CANGJIE_OK;
}

int cangjie_get_radicals(const Cangjie *cj, const char *keys, char **radicals) {
    size_t len = strlen(keys);
    size_t i;
    size_t j;
    int l = 0;

    // 3 bytes per radical for sure, plus one for the null terminator
    char *tmp = calloc(len * 3 + 1, sizeof(char));
    if (tmp == NULL) {
        return CANGJIE_NOMEM;
    }

    // Search for the radical of each key
    for (i = 0; i < len; i++) {
        char *radical;
        int ret = cangjie_get_radical(cj, keys[i], &radical);
        if (ret != CANGJIE_OK) {
            free(tmp);
            return ret;
        }
        for (j = 0; j < strlen(radical); j++) {
            tmp[l + j] = radical[j];
        }
        l += strlen(radical);
    }

    // terminate the string
    tmp[l] = '\0';

    // return the result
    *radicals = tmp;

    return CANGJIE_OK;
}

int cangjie_is_input_key(const Cangjie *cj, const char key) {
    if (key < 'a' || key > 'z') {
        return CANGJIE_INVALID;
    }

    return CANGJIE_OK;
}

int cangjie_free(Cangjie *cj) {
    sqlite3_close(cj->db);
    if (cj->cj_query != NULL) {
        free(cj->cj_query);
    }
    if (cj->cj_query_wildcard != NULL) {
        free(cj->cj_query_wildcard);
    }
    free(cj);

    return CANGJIE_OK;
}
