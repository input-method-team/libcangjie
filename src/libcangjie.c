/* Copyright (c) 2013 - The libcangjie authors.
 *
 * This file is part of libcangjie.
 *
 * libcangjie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libcangjie is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with libcangjie.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "libcangjie.h"

#ifndef LIBCANGJIE_VERSION
    #define LIBCANGJIE_VERSION "dev"
#endif

#ifndef LIBCANGJIE_VERSION_MAJOR
    #define LIBCANGJIE_VERSION_MAJOR 0
#endif

#ifndef LIBCANGJIE_VERSION_MINOR
    #define LIBCANGJIE_VERSION_MINOR 0
#endif

#ifndef LIBCANGJIE_VERSION_PATCH
    #define LIBCANGJIE_VERSION_PATCH 0
#endif

/**
 * Get the version of the library.
 *
 * @return The version string of the library.
 */
const char *libcangjie_version(void) { return LIBCANGJIE_VERSION; }

/**
 * Version compatibility check.
 *
 * Compare the version of the library with the given version numbers.
 * See if the given semver version numbers is greater than or equal to the
 * version of the library.
 *
 * @param cj The Cangjie instance.
 * @param major The major version number.
 * @param minor The minor version number.
 * @param patch The patch version number.
 *
 * @return 1 if the version is compatible, 0 otherwise.
 */
int libcangjie_check_version_gte(int32_t major, int32_t minor, int32_t patch) {
    if (major > LIBCANGJIE_VERSION_MAJOR) {
        return 1;
    }
    if (major == LIBCANGJIE_VERSION_MAJOR) {
        if (minor > LIBCANGJIE_VERSION_MINOR) {
            return 1;
        }
        if (minor == LIBCANGJIE_VERSION_MINOR &&
            patch >= LIBCANGJIE_VERSION_PATCH) {
            return 1;
        }
    }
    return 0;
}
